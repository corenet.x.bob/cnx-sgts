import { sendMessage } from '../src/tg.mjs'
import { handleMessage } from '../src/bot.mjs'

/**
 * @param {Request} req
 */
export default async (req) => {
	if (req.method !== 'POST') return new Response('not allowed', { status: 405 })
	const body = await req.json()
	if (!body?.message?.text) return new Response('', { status: 400 })
	const {
		update_id,
		message: { chat, from: _from, entities, ...msg },
	} = body

	console.log('msg', update_id, `${chat?.id} ${chat?.username}`, msg, entities)

	let reply = '?'

	try {
		reply = await handleMessage(chat.id, msg.text)
	} catch (err) {
		reply = `${err}`
	}

	try {
		const resp = await sendMessage(chat.id, reply)
		const rBod = await resp.json()
		console.log('tg ok', rBod?.ok)
	} catch (err) {
		console.error(err)
	}

	return new Response('ok')
}

export const config = {
	path: '/tg-webhook',
}
