const token = Netlify.env.get('TG_TOKEN')

/**
 * @param {string} path
 * @param {RequestInit} opts
 */
export const api = (path, opts) => fetch(`https://api.telegram.org/bot${token}/${path}`, { ...opts })

/**
 * @param {string} chat_id
 * @param {string} text
 * @param {Record<string, string>} [opts]
 */
export const sendMessage = (chat_id, text, opts) => api('sendMessage', {
	method: 'POST',
	headers: { 'content-type': 'application/json' },
	body: JSON.stringify({
		chat_id, text,
		parse_mode: 'MarkdownV2',
		...opts,
	}),
})
