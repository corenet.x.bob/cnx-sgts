import fetch from 'node-fetch'

const ctype = 'content-type'
const appJSON = 'application/json'

const apiURL = `https://${process.env.API_DOMAIN}/api`
const authorization = `Bearer ${process.env.API_TOKEN}`

export const api = (path, opts) => fetch(`${apiURL}/v4/${path}`, {
	...opts,
	headers: {
		authorization,
		...opts?.headers,
	},
}).then((r) => {
	switch (r.headers.get(ctype)) {
		case appJSON: return r.json()
		case 'text/plain': return r.text()
		default: return r
	}
})

export const gql = (query) => fetch(`${apiURL}/graphql`, {
	method: 'POST',
	headers: {
		authorization,
		[ctype]: appJSON,
	},
	body: JSON.stringify({ query }),
}).then((r) => r.json())
