const CNX_ES_REGEX = /^[0-9A-Z]{10}-[0-9A-Z]{5}-[0-9A-Z]{7}$/

/**
 * @param {string} maybeES
 */
const parseES = (maybeES) => {
	if (!maybeES) throw new Error('ES cannot be empty')
	if (!CNX_ES_REGEX.test(maybeES)) throw new Error(`invalid ES: \`${maybeES}\``)
	return maybeES
}

/**
 * @param {unknown} payload
 */
const createJob = async (payload) => {
	const body = JSON.stringify(payload)
	console.log('creating job', body)
	// call lambda
}

/**
 * @param {string} chatId
 * @param {string} msg
 */
export const handleMessage = async (chatId, msg) => {
	if (msg?.[0] !== '/') return '?'

	const [cmd, ...args] = msg.slice(1).split(' ')

	switch (cmd) {
		case 'sub_resend': {
			const es = parseES(args[0])
			await createJob({
				task: 'retrigger-send-submission',
				refNumber: es,
				chatId,
			})
			return `Retriggering submission package for \`${es}\``
		}
		case 'sub_retrieve': {
			const es = parseES(args[0])
			await createJob({
				task: 'retrieve-submission-package',
				refNumber: es,
				chatId,
			})
			return `Retrieving submission package for \`${es}\``
		}
		case 'sub_delete': {
			const es = parseES(args[0])
			await createJob({
				task: 'delete-submission',
				refNumber: es,
				chatId,
			})
			return `Sending request to delete submission \`${es}\``
		}
		default:
			throw new Error(`unknown command: ${cmd}`)
	}
}
